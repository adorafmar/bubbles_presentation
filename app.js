var port = require('./config.js').get(process.env.NODE_ENV)['port'];
var express = require('express');
var path = require('path');
var app = express(); 

app.set('port', port);

app.use(express.static(path.join(__dirname, 'config')));
app.use(express.static(path.join(__dirname, 'public')));

app.get('/', function (req, res) {
	res.set('Access-Control-Allow-Origin', process.env.ACAO);
	console.log(process.env.ACAO)
})

var server = app.listen(app.get('port'), function() {
  var port = server.address().port;
  console.log('Magic happens on port ' + port);
});